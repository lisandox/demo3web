<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'DEMO3WEB' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

if ( !defined('WP_CLI') ) {
    define( 'WP_SITEURL', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
    define( 'WP_HOME',    $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
}



/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '9T2jS5rtI8oh8n5oMn0I6jTRkoTEgRhtPIYG4fxhl1MCTNTKQQY3negDT9i0VERX' );
define( 'SECURE_AUTH_KEY',  'mJJbvbnOVrja4wS4UDMds0yCVewXDHCXUDftbMYZsRKoFgO3ADLfpuc941X7S0Uz' );
define( 'LOGGED_IN_KEY',    'SkWum4KvXjBW4QyZ9XezBLgyQ6Yv26hvTR3TBDa3wReBgUrGHIgZnMZQPnYGJKrm' );
define( 'NONCE_KEY',        'EZW8EoT0HrtqyVBXxvp5dpQWLEHaxBUDYrXWxWJbvhe7Sc3sjqdlsmf4Il8i8ZI5' );
define( 'AUTH_SALT',        'CcVq6k5NtwgykkyD5vCBpfNQ2o5QhOUkJtBAuWCVBh2jjxWMypOKcIFIULOVzHFE' );
define( 'SECURE_AUTH_SALT', 'THjqvJGyN0YCOUPx7AlQ5PDkWwgiy4AhDmioplrSIR1fUxBjaTTT4HZVzvHtUEtw' );
define( 'LOGGED_IN_SALT',   '2BWkHB5ZyviN6ppjiD24G1nUnjwn5MhNGlt9Z8t5T65AMbV57itSwtTIRUjlL8Im' );
define( 'NONCE_SALT',       'HSMfipDtyF8CAQIkb1W7eXRvtdRmfEDaWxOF3IOA3iPWN9Y0UOZceb0xIgf01syo' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
